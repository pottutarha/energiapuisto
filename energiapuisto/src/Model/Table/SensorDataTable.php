<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sensordata Model
 *
 * @method \App\Model\Entity\Sensordata get($primaryKey, $options = [])
 * @method \App\Model\Entity\Sensordata newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Sensordata[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Sensordata|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sensordata|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sensordata patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Sensordata[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Sensordata findOrCreate($search, callable $callback = null, $options = [])
 */
class SensordataTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sensordata');
        $this->setDisplayField('name');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->requirePresence('id', 'create')
            ->allowEmptyString('id', false);

        $validator
            ->integer('sensor_id')
            ->requirePresence('id', 'create')
            ->allowEmptyString('id', false);

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);
        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->allowEmptyString('type', false);

        $validator
            ->integer('value')
            ->requirePresence('value', 'create')
            ->allowEmptyString('value', false);

        $validator
            ->integer('groupid')
            ->requirePresence('groupid', 'create')
            ->allowEmptyString('groupid', false);

        $validator
            ->datetime('date')
            ->requirePresence('date', 'create')
            ->allowEmptyDate('date', false);

        return $validator;
    }
}
