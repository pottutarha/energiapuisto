<?php
use Cake\Cache\Cache;
use Cake\Utility\Hash;
use Cake\Http\Client;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\I18n\Date;


$this->append('script', $this->Html->script([
	'/gentelella/vendors/Chart.js/dist/Chart.bundle.js'

]));
$dataForCharts = array();
$namesForCharts = array();
foreach ($sensordata as $value) {
	$date = date("d-m-Y H:i:s", strtotime($value['date']));
        $dataForCharts[$value->id] = $value['value'];
        $namesForCharts[$value->id] = $date;
}
$dataForCharts = array_values($dataForCharts);
$namesForCharts = array_values($namesForCharts);
?>
<div style="text-align: center">
<p style="font-size: 32px; position: center;"><?php echo $sensordata[0]['name']; ?> tiedot </p>
</div>
<div class="chart-container" style="text-align: center">
   <div class="row row-centered">
    	<div id="1" class="col-lg-12 col-centered">
    	<div style="border-bottom: 2px solid rgba(42, 63, 84, 0.1);"><h3>Lämpötiloja</h3></div>
        <canvas id="canvas" style="padding-right: 10px;"></canvas>
    </div>
</div>
</div>
<div class="container">
   
    <div id="4" class="col-lg-12 col-centered" style="text-align: center;">
        <div style="border-bottom: 2px solid rgba(42, 63, 84, 0.1);"><h3>Hälytykset</h3></div>
        <div class="dataTables_wrapper" style="padding-top: 10px; width: 100%;"> 
        <table class="table">
        <thead>
                <th scope="col" style="text-align: center;"><?php echo 'Id'; ?></th>
                <th scope="col" style="text-align: center;"><?php echo 'Nimi'; ?></th>
                <th scope="col" style="text-align: center;"><?php echo 'Arvo'; ?></th>
                <th scope="col" style="text-align: center;"><?php echo 'Hälytys'; ?></th>
                <th scope="col" style="text-align: center;"><?php echo 'Päivämäärä'; ?></th>
                <th scope="col" class="actions" style="text-align: center;"><?= __('Toiminnot') ?></th>
        </thead>
        <tbody>
        
        <?php
        foreach($alerts as $value): ?>
            <tr>
                <td><?php echo $value['sensorid']; ?></td>
                <td><?php echo $value['name']; ?></td>
                <td><?php echo $value['value']; ?></td>
                <td><?php echo $value['alert_type']; ?></td>
                <td><?php echo $value['date']; ?></td>
                <td><?= $this->Html->link(
                        'Kuittaa hälytys',
                        ['class' => 'text', 'controller' => 'SensorData', 'action' => 'removeAlert',$value['id'], 'style' => 'color: #b20000;']
                    ); ?> 
</td>
            </tr>
       <?php endforeach; ?>
         </tbody>
        </table>
        
        </div>
    </div>
   </div>
<script type="text/javascript">
 window.onload = function() {
var ctx = document.getElementById("canvas").getContext("2d");
window.myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: <?php echo json_encode($namesForCharts) ?>,
        datasets: [{
            label: 'Lämpötila',
            data: <?php echo json_encode($dataForCharts) ?>,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }],
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
}
</script>
