<?php
use Cake\Cache\Cache;
use Cake\Utility\Hash;
use Cake\Http\Client;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\I18n\Date;


$this->append('script', $this->Html->script([
	'/gentelella/vendors/Chart.js/dist/Chart.bundle.js'

]));

?>
<?php
$dataForCharts = array();
$namesForCharts = array();
foreach ($sensordata as $value) {
    if ($value['value'] != 0) {
        $dataForCharts[$value->id] = $value['value'];
        $namesForCharts[$value->id] = $value['name'];
    }
}
$dataForCharts = array_values($dataForCharts);
$namesForCharts = array_values($namesForCharts);
// dd($dataForCharts);

 ?>
<!-- <div><?php echo $this->Html->link("Demodatan luonti", array('controller' => 'sensordata','action'=> 'add'), array( 'class' => 'button')); ?> </div> -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<div style="text-align: center">
<p style="font-size: 32px; position: center;">Yleiskatsaus</p>
</div>
<div class="container">
    <div class="row row-centered">
    	<div id="1" class="col-md-6 col-centered">
    	<div style="border-bottom: 2px solid rgba(42, 63, 84, 0.1);"><h3>Lämpötiloja</h3></div>
        <canvas id="canvas" style="padding-right: 10px;"></canvas>
    </div>


    <div id="4" class="col-md-6 col-centered">
        <div style="border-bottom: 2px solid rgba(42, 63, 84, 0.1);"><h3>Hälytykset/äkilliset lämpötilamuutokset</h3></div>
        <div class="dataTables_wrapper" style="padding-top: 10px; width: 100%;"> 
        <table class="table">
        <thead>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name','Nimi') ?></th>
                <th scope="col"><?= $this->Paginator->sort('value','Arvo') ?></th>
                <th scope="col"><?= $this->Paginator->sort('alert_type','Hälytys') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date','Päivämäärä') ?></th>
                <th scope="col" class="actions"><?= __('Toiminnot') ?></th>
        </thead>
        <tbody>
        
        <?php
        foreach($alert as $value): ?>
            <tr>
                <td><?= $this->Number->format($value['id']) ?></td>
                <td><?= h($value['name']) ?></td>
                <td><?= $this->Number->format($value['value']) ?></td>
                <td><?= h($value['alert_type']) ?></td>
                <td><?= h($value['date']) ?></td>
                <td><?= $this->Html->link(
                        'Kuittaa hälytys',
                        ['class' => 'text', 'controller' => 'SensorData', 'action' => 'removeAlert',$value['id'], 'style' => 'color: #b20000;']
                    ); ?> 
</td>
            </tr>
       <?php endforeach; ?>
         </tbody>
        </table>
        <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Alkuun')) ?>
            <?= $this->Paginator->prev('< ' . __('Edellinen')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Seuraava') . ' >') ?>
            <?= $this->Paginator->last(__('Viimeinen') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Näytetään sivu {{page}}/{{pages}}, Näytetään {{current}} tulosta yhteensä {{count}} tuloksesta')]) ?></p>
    </div>
        </div>
    </div>
   </div>
    <div id="Kuormitus" class="col-md-12 col-centered">
        <div style="border-bottom: 2px solid rgba(42, 63, 84, 0.1);"><h3>Lisää dataa</h3></div>
        <h1> Tänne voi iskeä mitä tarvitaan / halutaan pikaista tarkastelua varten </h1>
    </div>
   </div>

<script>
	var config = {
        type: 'radar',
        data: {
            labels: ["Tärkeä Asia 1", "Juttu 2", "Asia3"],
            datasets: [{
                label: "Tärkeä Asia 1",
                backgroundColor: "rgba(220,220,220,0.2)",
                pointBackgroundColor: "rgba(220,220,220,1)",
                data: [],
            }, {
                label: 'Juttu 2 piilotettu',
                hidden: true,
                data: [34,4,55],
            }, {
                label: "Asia 3",
                backgroundColor: "rgba(151,187,205,0.2)",
                pointBackgroundColor: "rgba(151,187,205,1)",
                hoverPointBackgroundColor: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [366,4,5]
            },]
        },
        options: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: '"radar" graafi'
            },
            scale: {
              reverse: false,
              ticks: {
                beginAtZero: true
              }
            }
        }
    };

window.onload = function() {
var ctx = document.getElementById("canvas").getContext("2d");
window.myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: <?php echo json_encode($namesForCharts) ?>,
        datasets: [{
            label: 'Lämpötila',
            data: <?php echo json_encode($dataForCharts) ?>,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }],
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

var ctc = document.getElementById("canvas2").getContext("2d");
window.myChart = new Chart(ctc, {
    type: 'line',
    data: {
        labels: <?php echo json_encode($namesForCharts) ?>,
        datasets: [{
            label: 'Testidatalla luotuja lämpötiloja',
            data: [<?php echo json_encode($dataForCharts) ?>],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
var ctxx = document.getElementById("canvas3").getContext("2d");
window.myChart = new Chart(ctxx, {
    type: 'doughnut',
    data: {
        labels: <?php echo json_encode($namesForCharts) ?>,
        datasets: [{
            label: 'Testidatalla luotuja lämpötiloja',
            data: <?php echo json_encode($dataForCharts) ?>,
            backgroundColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(153, 102, 25, 1)',
                'rgba(153, 12, 255, 1)',
                'rgba(13, 102, 255, 1)',
            ],
            borderColor: [
                'rgba(255,99,132,1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
        window.myRadar = new Chart(document.getElementById("canvas4"), config);
    
};
</script>
