<?php
use Cake\Cache\Cache;
use Cake\Utility\Hash;
use Cake\Http\Client;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\I18n\Date;


$this->append('script', $this->Html->script([
	'/gentelella/vendors/Chart.js/dist/Chart.bundle.js'

]));

?>
<?php
$alert = [];
foreach ($alerts as $key => $value) {
    $alert[$value['id']] = $value['id'];
}

$dataForCharts = [];
$sensorid = [];
$namesForCharts = [];
foreach ($sensordata as $value) {
    if (!in_array($value->sensor_id, $sensorid)) {
    $sensorid[$value->name] = $value->sensor_id;
    }
    $dataForCharts[$value->name] = $value->value;
    $namesForCharts[$value->name] = $value->value;
}
$groups = [];
foreach ($groupdata as $key => $value) {
    $groups[$value['id']] = $value['name'];
}
$dataForCharts = array_values($dataForCharts);
$namesForCharts = array_keys($namesForCharts);
 ?>
 <button class="btn btn-success" data-toggle="modal" data-target="#switch-group">Valitse ryhmä</button>
<?php if (isset($groupInfo)): ?>
<div> <button class="btn btn-success" data-toggle="modal" data-target="#show-group">Näytä ryhmän tiedot</button> </div>
<?php endif; ?>
<div class="modal" id="switch-group" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Valitse ryhmä</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
        <?php 
            echo $this->Form->create(null, ['action' => 'sensors']);
            echo $this->Form->control("id",['options' => $groups, 'label' => false]);
        ?> 
      </div>
      <div class="modal-footer">
        <?php
        echo $this->Form->submit('Suodata', ['class' => 'btn btn-success']);
        echo $this->Form->end();
        ?> 
      </div>
    </div>
  </div>
</div>
<div class="modal" id="show-group" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">Ryhmän tiedot</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table">
        <thead>
                <th scope="col" style="text-align: center;"><?php echo "Tunniste"; ?></th>
                <th scope="col" style="text-align: center;"><?php echo "Nimi" ?></th>
                <th scope="col" style="text-align: center;"><?php echo "Minimiarvo" ?></th>
                <th scope="col" style="text-align: center;"><?php echo "Maksimiarvo" ?></th>
                <th scope="col" style="text-align: center;"><?php echo "Päivämäärä" ?></th>
        </thead>
        <tbody>
            <tr>
                <td><?php echo $groupInfo['id'] ?></td>
                <td><?php echo $groupInfo['name'] ?></td>
                <td><?php echo $groupInfo['value_min'] ?></td>
                <td><?php echo $groupInfo['value_max'] ?></td>
                <td><?php echo $groupInfo['date'] ?></td>
                <td> <?= $this->Html->link(__('Muokkaa'), ['controller' => 'groups', 'action' => 'edit', $groupInfo['id']]) ?> </td>
            </tr>
         </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Sulje </button>
      </div>
    </div>
  </div>
</div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<div style='text-align: center;'>
<p style="font-size: 32px;">Sensorit </p>
<p style="font-size: 12px;">Näytetään viimeisimmät 10 sensoridataa </p>
</div>
<div class="chart-container" style="text-align: center">
    <div class="row row-centered">
    	<div id="1" class="col-md-6 col-centered">
    	<div style="border-bottom: 2px solid rgba(42, 63, 84, 0.1);"><h3>Lämpötiloja</h3></div>
        <canvas id="canvas" style="padding-right: 10px;"></canvas>
    </div>
        <div id="2" class="col-md-5 col-centered">
         	<div style="border-bottom: 2px solid rgba(42, 63, 84, 0.1);"><h3>Muutoksia</h3></div>
            <canvas id="canvas2" style="padding-right: 10px;"></canvas>
        </div>
   </div>
   </div>
<div style='text-align: center;'>
<?php if(isset($groupInfo)): ?>
<p style="font-size: 32px;">Ryhmään kuuluvat sensorit</p>
<div class="dataTables_wrapper" style="padding-top: 10px; width: 100%;"> 
        <table class="table">
        <thead>
                <th scope="col" style="text-align: center;"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col" style="text-align: center;"><?= $this->Paginator->sort('sensor_id','Sensorin tunniste') ?></th>
                <th scope="col" style="text-align: center;"><?= $this->Paginator->sort('name','Sensorin nimi') ?></th>
                <th scope="col" style="text-align: center;"><?= $this->Paginator->sort('value','Arvo') ?></th>
                <th scope="col" style="text-align: center;"><?= $this->Paginator->sort('groupid','Ryhmän tunniste') ?></th>
                <th scope="col" style="text-align: center;"><?= $this->Paginator->sort('date','Päivämäärä') ?></th>
        </thead>
        <tbody>
        <?php
        foreach($sensordata as $value): ?>
            <?php if (array_key_exists($value['id'], $alert)): ?>
            <tr style="background-color: #b20000; color: white;">
            <?php endif; ?>
             <?php if (!in_array($value['id'], $alert)): ?>
                <tr>
                <?php endif; ?>
                <td><?= $this->Number->format($value['id']) ?></td>
                <td><?= $this->Number->format($value['sensor_id']) ?></td>
                <td><?= h($value['name']) ?></td>
                <td><?= $this->Number->format($value['value']) ?></td>
                <td><?= $this->Number->format($value['groupid']) ?></td>
                <td><?= h($value['date']) ?></td>
            </tr>
       <?php endforeach; ?>
         </tbody>
        </table>
</div>

<?php endif; ?>



<script>
	var config = {
        type: 'radar',
        data: {
            labels: ["Tärkeä Asia 1", "Juttu 2", "Asia3"],
            datasets: [{
                label: "Tärkeä Asia 1",
                backgroundColor: "rgba(220,220,220,0.2)",
                pointBackgroundColor: "rgba(220,220,220,1)",
                data: <?php echo json_encode($dataForCharts); ?>
            }, {
                label: 'Juttu 2 piilotettu',
                hidden: true,
                data: [34,4,55],
            }, {
                label: "Asia 3",
                backgroundColor: "rgba(151,187,205,0.2)",
                pointBackgroundColor: "rgba(151,187,205,1)",
                hoverPointBackgroundColor: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [366,4,5]
            },]
        },
        options: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: '"radar" graafi'
            },
            scale: {
              reverse: false,
              ticks: {
                beginAtZero: true
              }
            }
        }
    };

window.onload = function() {
var ctx = document.getElementById("canvas").getContext("2d");
window.myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: <?php echo json_encode($namesForCharts); ?>,
        datasets: [{
            label: 'Lämpötila',
            data: <?php echo json_encode($dataForCharts); ?>,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',

                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

var ctc = document.getElementById("canvas2").getContext("2d");
window.myChart = new Chart(ctc, {
    type: 'line',
    data: {
        labels: <?php echo json_encode($namesForCharts) ?>,
        datasets: [{
            label: 'Testidatalla luotuja lämpötiloja',
            data: <?php echo json_encode($dataForCharts) ?>,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
var ctxx = document.getElementById("canvas3").getContext("2d");
window.myChart = new Chart(ctxx, {
    type: 'doughnut',
    data: {
        labels: <?php echo json_encode($namesForCharts) ?>,
        datasets: [{
            label: 'Testidatalla luotuja lämpötiloja',
            data: <?php echo json_encode($dataForCharts) ?>,
            backgroundColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(153, 102, 25, 1)',
                'rgba(153, 12, 255, 1)',
                'rgba(13, 102, 255, 1)',
            ],
            borderColor: [
                'rgba(255,99,132,1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
        window.myRadar = new Chart(document.getElementById("canvas4"), config);
    
};
</script>

