<h1>Add random data</h1>
<?php
use Cake\I18n\Time;
use Cake\I18n\Date;

$random = rand(1,10);
    echo $this->Form->create($sensordata);
    echo $this->Form->control('id' ,['value' => $random]);
    echo $this->Form->control('name', ['value' => generateRandomString()]);
    echo $this->Form->control('value', ['value' => $random]);
    echo $this->Form->control('date', ['value' => new date('now')]);
    echo $this->Form->button(__('Save data'));
    echo $this->Form->end();



function generateRandomString($length = 10) {
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

?>
