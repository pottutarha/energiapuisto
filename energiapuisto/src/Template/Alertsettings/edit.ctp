<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Alertsetting $alertsetting
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $alertsetting->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $alertsetting->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Alertsettings'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="alertsettings form large-9 medium-8 columns content center">
    <?= $this->Form->create($alertsetting) ?>
    <fieldset>
        <legend><?= __('Edit Alertsetting') ?></legend>
        <?php
            echo $this->Form->control('alertgroup');
            echo $this->Form->control('value');
            echo $this->Form->control('groupname');
            echo $this->Form->control('date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
