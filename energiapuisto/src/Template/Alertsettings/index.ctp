<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Alertsetting[]|\Cake\Collection\CollectionInterface $alertsettings
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <button><?= $this->Html->link(__('Uusi hälytysryhmä'), ['action' => 'add']) ?></button>
    </ul>
</nav>
<div class="alertsettings index large-9 medium-8 columns content">
    <h3><?= __('Alertsettings') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('alertgroup') ?></th>
                <th scope="col"><?= $this->Paginator->sort('value') ?></th>
                <th scope="col"><?= $this->Paginator->sort('groupname') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($alertsettings as $alertsetting): ?>
            <tr>
                <td><?= $this->Number->format($alertsetting->id) ?></td>
                <td><?= $this->Number->format($alertsetting->alertgroup) ?></td>
                <td><?= h($alertsetting->groupname) ?></td>
                <td><?= h($alertsetting->date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $alertsetting->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $alertsetting->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $alertsetting->id], ['confirm' => __('Are you sure you want to delete # {0}?', $alertsetting->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
