<?php
use Cake\I18n\Time;
use Cake\I18n\Date;
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Alertsetting $alertsetting
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Alertsettings'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="alertsettings form large-9 medium-8 columns content center">
    <?= $this->Form->create($alertsetting) ?>
    <fieldset>
        <legend><?= __('Lisää hälytysryhmä') ?></legend>
            <h2>Erottele hälytysryhmään kuuluvat laitteet pilkulla!</h2>
            <div> Ryhmän id: <?php echo $this->Form->control('alertgroup', ['label' => false, 'type' => 'int']); ?></div>
           <div> Nimeä hälytysryhmä: <?php echo $this->Form->control('groupname', ['label' => false, 'type' => 'text']); ?></div>
           <div> Hälytysraja: <?php echo $this->Form->control('value', ['label' => false, 'type' => 'text']); ?></div>
           <div> Päivämäärä: <?php echo $this->Form->control('date', ['label' => false,'value' => new date('now')]); ?></div>

    </fieldset>
    <div><?= $this->Form->button(__('Submit')) ?></div>
    <?= $this->Form->end() ?>
</div>
