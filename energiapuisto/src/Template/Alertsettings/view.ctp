<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Alertsetting $alertsetting
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Alertsetting'), ['action' => 'edit', $alertsetting->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Alertsetting'), ['action' => 'delete', $alertsetting->id], ['confirm' => __('Are you sure you want to delete # {0}?', $alertsetting->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Alertsettings'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Alertsetting'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="alertsettings view large-9 medium-8 columns content">
    <h3><?= h($alertsetting->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Groupname') ?></th>
            <td><?= h($alertsetting->groupname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($alertsetting->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Alertgroup') ?></th>
            <td><?= $this->Number->format($alertsetting->alertgroup) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($alertsetting->date) ?></td>
        </tr>
    </table>
</div>
