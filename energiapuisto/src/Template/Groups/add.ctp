<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Group $group
 */
?>
<style>
#inputs {
    padding-left: 10px;
}
#back {
    color: white;
}
</style>
  <button class='btn btn-success'>&#8592; <?= $this->Html->link(__('Takaisin selaukseen'), ['action' => 'index'], ['id' => 'back']) ?></button>
<div class="groups form large-9 medium-8 columns content">
    <?= $this->Form->create($group) ?>
    
        <legend><?= __('Lisää hälytysryhmä') ?></legend>
        <?php
            echo '<h3>Nimi</h3>';
            echo $this->Form->control('name', ['label' => false, 'id' => 'inputs']);
            echo '<h3>Minimi arvo hälytykselle</h3>';
            echo $this->Form->control('value_min', ['label' => false, 'id' => 'inputs']);
            echo '<h3>Maksimi arvo hälytykselle</h3>';
            echo $this->Form->control('value_max', ['label' => false, 'id' => 'inputs']);
            echo '<h3>Päivämäärä</h3>';
            echo $this->Form->control('date', ['label' => false, 'id' => 'inputs']);
        ?>
    <?= $this->Form->button(__('Tallenna', ['type' => 'btn btn-success'])) ?>
    <?= $this->Form->end() ?>
</div>
