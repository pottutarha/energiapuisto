<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Group[]|\Cake\Collection\CollectionInterface $groups
 */
?>
<style>
th {
    padding-right: 5px;
}
#add {
    color: white;
}
</style>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav pull-right">
         <button class="btn btn-success" style="color: white;">   <?= $this->Html->link(__('Lisää uusi ryhmä'), ['action' => 'add'], ['id' => 'add']) ?> </button>
    </ul>
</nav>
<div class="groups index large-9 medium-8 columns content">
    <h3><?= __('Hallitse hälytysryhmiä') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('value_min') ?></th>
                <th scope="col"><?= $this->Paginator->sort('value_max') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($groups as $group): ?>
            <tr>
                <td><?= $this->Number->format($group->id) ?></td>
                <td><?= h($group->name) ?></td>
                <td><?= $this->Number->format($group->value_min) ?></td>
                <td><?= $this->Number->format($group->value_max) ?></td>
                <td><?= h($group->date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $group->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $group->id], ['confirm' => __('Are you sure you want to delete # {0}?', $group->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Näytetään sivu {{page}}/{{pages}}, <br> Näytetään {{current}} tulosta yhteensä {{count}} tuloksesta')]) ?></p>
    </div>
</div>
