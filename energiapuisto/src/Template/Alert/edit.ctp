<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Alert $alert
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $alert->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $alert->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Alert'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="alert form large-9 medium-8 columns content">
    <?= $this->Form->create($alert) ?>
    <fieldset>
        <legend><?= __('Muokkaa hälytystä') ?></legend>
        <?php
            echo $this->Form->control('sensorid');
            echo $this->Form->control('name');
            echo $this->Form->control('value');
            echo $this->Form->control('alert_type');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Lähetä')) ?>
    <?= $this->Form->end() ?>
</div>
