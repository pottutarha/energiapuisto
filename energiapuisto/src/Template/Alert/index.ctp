<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Alert[]|\Cake\Collection\CollectionInterface $alert
 */
?>

<div class="alert index large-9 medium-8 columns content">
    <h3><?= __('Kuittaa hälytyksiä') ?></h3>
   <div class="dataTables_wrapper" style="padding-top: 10px; width: 100%;"> 
        <table class="table">
        <thead>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name','Nimi') ?></th>
                <th scope="col"><?= $this->Paginator->sort('value','Arvo') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sensorid','Sensorin id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('alert_type','Hälytyksen tyyppi') ?></th>
                <th scope="col" class="actions"><?= __('Toiminnot') ?></th>
        </thead>
        <tbody>
        
        <?php
        foreach($alert as $value): ?>
            <tr>
                <td><?= $this->Number->format($value['id']) ?></td>
                <td><?= h($value['name']) ?></td>
                <td><?= $this->Number->format($value['value']) ?></td>
                <td><?= h($value['sensorid']) ?></td>
                <td><?= h($value['alert_type']) ?></td>
                <td><?= $this->Html->link(
                        'Kuittaa hälytys',
                        ['class' => 'text', 'controller' => 'SensorData', 'action' => 'removeAlert',$value['id'], 'style' => 'color: #b20000;']
                    ); ?> 
</td>
            </tr>
       <?php endforeach; ?>
         </tbody>
        </table>
        <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
        </div>
</div>
