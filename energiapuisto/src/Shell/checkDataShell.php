<?php 
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Log\Log;
use Cake\Console\Helper;
use Cake\Console\ConsoleIo;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;

class checkDataShell extends Shell
{ 


	public function main($truncate = false) {
	if ($truncate) {
		$conn = ConnectionManager::get('default');
		$conn->execute('TRUNCATE TABLE alert');
	}
	$sensorData = $this->loadModel('Sensordata');
	$alert = $this->loadModel('Alert');
	$alertGroup = $this->loadModel('Groups');
	$sensorData = $this->Sensordata->find('all')->toArray();
	$sensorDataAlerts = $this->Alert->find('all')->toArray();

	$amountOfAlerts = count($sensorDataAlerts);
	$amountOfAlerts = $amountOfAlerts + 1;

	$alerts = array();
	$alertsInDb = array();
	foreach ($sensorDataAlerts as $value) {
		$alertsInDb[$value['sensorid']] = $value['sensorid'];
	}
	foreach($sensorData as $key => $value) {
		$groups = $this->Groups->find('all')->where(['id' => $value['groupid']])->toArray();
		$min_value = $groups[0]['value_min'];
		$max_value = $groups[0]['value_max'];
		if ($value['value'] <= $min_value) {
				if ($value['type'] == 'temp') {
				$alerts[$amountOfAlerts] = [
					'id' => $amountOfAlerts,
					'sensorid' => $value['id'],
					'name' =>	$value['name'],
					'value' => $value['value'],
					'date' => $value['date'],
					'alert_type' => 'Alhainen lämpötila',
				];
				$amountOfAlerts++;
			} else {
				$alerts[$amountOfAlerts] = [
					'id' => $amountOfAlerts,
					'sensorid' => $value['id'],
					'name' =>	$value['name'],
					'value' => $value['value'],
					'date' => $value['date'],
					'alert_type' => 'Kosteustaso matala',
				];
				$amountOfAlerts++;
			}
		} elseif ($value['value'] >= $max_value) {
			if ($value['type'] == 'temp') {
				$alerts[$amountOfAlerts] = [
					'id' => $amountOfAlerts,
					'sensorid' => $value['id'],
					'name' =>	$value['name'],
					'value' => $value['value'],
					'date' => $value['date'],
					'alert_type' => 'Liian korkea lämpötila',
				];
				$amountOfAlerts++;
			} else {
				$alerts[$amountOfAlerts] = [
					'id' => $amountOfAlerts,
					'sensorid' => $value['id'],
					'name' =>	$value['name'],
					'value' => $value['value'],
					'date' => $value['date'],
					'alert_type' => 'Kosteustaso korkea',
				];
				$amountOfAlerts++;
			}
			
		} elseif ($value['value'] == NULL || $value['value'] == 0) {
			$alerts[$amountOfAlerts] = [
					'id' => $amountOfAlerts,
					'sensorid' => $value['id'],
					'name' =>	$value['name'],
					'value' => $value['value'],
					'date' => $value['date'],
					'alert_type' => 'Sensori ei toiminnassa',
				];
				$amountOfAlerts++;
		}
	}

	$countAlerts = 0;
	foreach($alerts as $value) {
		$alertsToDB = $this->Alert->newEntity();
		$alertsToDB = $this->Alert->patchEntity($alertsToDB, $value);
		if ($this->Alert->save($alertsToDB)) {

		} else {
			$this->out('Error, aborting...');
			break;
		}
		$countAlerts++;
		
	}
	$this->out('Päivitetty hälytyslista, ' .$countAlerts. ' uutta hälytystä');

	// $email = new Email('gmail');
	// $email->from(['me@example.com' => 'Hälytys'])
 //    ->to('sposti@sposti.fi')
 //    ->subject('Sensorihälytys')
 //    ->send('hälytys');
	}
	// public function randomAlert(){
	// 	$phrases = [
	// 	'Alhainen lämpötila',
	// 	'Liian korkea lämpötila',
	// 	'Kosteustaso liian matala',
	// 	'Sensori ei toiminnassa',
	// 	];
	// 	return $phrases[array_rand($phrases)];
	// }
}
