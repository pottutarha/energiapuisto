<?php 
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Log\Log;
use Cake\Console\Helper;
use Cake\Console\ConsoleIo;
use Cake\Datasource\ConnectionManager;

class modifydataShell extends Shell
{ 


	public function main() {
		$this->addData(100, true);
	}

	//Funktio testidatan lisäykselle
	//$amount = luotavan datan määrä
	//$truncate = true tai false, true tyhjentää taulun ennen datan luomista
	public function addData($amount, $truncate = false) {
		if ($truncate === true) {
			$conn = ConnectionManager::get('default');
			$conn->execute('TRUNCATE TABLE sensordata');
			$this->out('Tyhjennetään taulu');
		}
		$sensorData = $this->loadModel('Sensordata');
		$sensorData = $this->Sensordata->find('all')->toArray();
		$lastId = count($sensorData);
		$newData = array();
		$newId = $lastId +1;
		$dataCreated = 1;
		$date = new \DateTime('now');

		while ($dataCreated <= $amount) {
			$randomSensor = rand(1,33);
			$randomPhrase = $this->randomString();
			if (strpos($randomPhrase,'Kosteushaistelija') !== false) {
				$type = 'humidity';
				$typeVal = rand(60, 100);
			} else {
				$type = 'temp';
				$typeVal = rand(-30, 40);
			}
			$newData[] = [
			'id' => $newId,
			'sensor_id' => $randomSensor,
			'name' => $randomPhrase,
			'value' => $typeVal,
			'groupid' => rand(1,6),
			'date' => $date,
			'type' => $type,
			];
			$newId++;
			$dataCreated++;
		}
	
	$amount = 0;
	foreach($newData as $value) {

		$newSensorData = $this->Sensordata->newEntity();
		$newSensorData = $this->Sensordata->patchEntity($newSensorData, $value);
		if ($this->Sensordata->save($newSensorData)) {
		} else {
			$this->out('Error, aborting...');
			break;
		}
		$amount++;
	}
	$this->out('Lisätty tietokantaan ' . $amount . ' arvoa');

	}

	public function randomString(){
		$phrases = [
		'Sensori',
		'Lämpösensori',
		'Kosteushaistelija',
		];
		$randomNum = rand(1,3000);
		return $phrases[array_rand($phrases)] .' '.$randomNum;
	}
}