<?php
namespace App\Controller;

use App\Controller\AppController;
/**
 * Sensordata Controller
 *
 * @property \App\Model\Table\SensordataTable $Sensordata
 *
 * @method \App\Model\Entity\Sensordata[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SensordataController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public $paginate = ['limit' => 10 ];
    public function index()
    {
        $sensordata = $this->Sensordata->find('all', array('limit' => 10))->order('date ASC');
        $alert = $this->loadModel('Alert');
        $alert = $this->Alert->find('all')->toArray();
        $alert = $this->paginate($this->Alert);

        $this->set(compact('sensordata', 'alert'));
    }

    public function sensors()
    {
        $groupdata = $this->loadModel('Groups');
        $alerts = $this->loadModel('Alert');
        $alerts = $this->Alert->find('all');
        $groupdata = $this->Groups->find('all')->toArray();
        $groupid = $this->request->data;
        if ($groupid != NULL) {
            $sensordata = $this->Sensordata->find('all')->where(['groupid' => $groupid['id']])->toArray();
            $groupInfo = $this->Groups->find('all')->where(['id' => $groupid['id']])->first();
        } else {
            $sensordata = $this->paginate($this->Sensordata);
        }
        $this->set(compact('sensordata','groupdata', 'groupInfo','alerts'));

    }
    public function singlesensor($sensorid, $date = null)
    {
        if ($date == null) {
            $date = new \DateTime();
            $date = $date->modify('-2 weeks');
        }
        $alerts = $this->loadModel('Alert');
        $alerts = $this->Alert->find('all')
        ->where([
            'sensorid' => $sensorid,
            'date >' => $date
        ])->toArray();
        $sensordata = $this->loadModel('Sensordata');
        $sensordata = $this->Sensordata->find('all')
        ->where([
            'sensor_id' => $sensorid,
            'date >' => $date
        ])->toArray();
       
        $this->set(compact('sensordata','alerts'));

    }
    /**
     * View method
     *
     * @param string|null $id Sensordata id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sensordata = $this->Sensordata->get($id, [
            'contain' => []
        ]);

        $this->set('sensordata', $sensordata);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sensordata = $this->Sensordata->newEntity();
        if ($this->request->is('post')) {
            $sensordata = $this->Sensordata->patchEntity($sensordata, $this->request->getData());
            if ($this->Sensordata->save($sensordata)) {
                $this->Flash->success(__('Testidata tallennettu onnistuneesti.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Ei voitu tallentaa, yritä uudelleen.'));
        }
        $this->set(compact('sensordata'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sensordata id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sensordata = $this->Sensordata->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sensordata = $this->Sensordata->patchEntity($sensordata, $this->request->getData());
            if ($this->Sensordata->save($sensordata)) {
                $this->Flash->success(__('The sensordata has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sensordata could not be saved. Please, try again.'));
        }
        $this->set(compact('sensordata'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sensordata id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sensordata = $this->Sensordata->get($id);
        if ($this->Sensordata->delete($sensordata)) {
            $this->Flash->success(__('The sensordata has been deleted.'));
        } else {
            $this->Flash->error(__('The sensordata could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function removeAlert($id = null) {
        $alert = $this->loadModel('Alert');
        $this->autoRender = false;
        $this->request->allowMethod(['post', 'delete', 'get']);
        $alert = $this->Alert->get($id);
        if ($this->Alert->delete($alert)) {
             $this->Flash->success(__('Hälytys kuitattu.'));
        } else {
            $this->Flash->error(__('Kuittaus ei onnistunut, yritä uudelleen!'));
        }
        return $this->redirect($this->referer());
    }
}
