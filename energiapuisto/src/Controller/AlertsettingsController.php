<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Alertsettings Controller
 *
 * @property \App\Model\Table\AlertsettingsTable $Alertsettings
 *
 * @method \App\Model\Entity\Alertsetting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AlertsettingsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $alertsettings = $this->paginate($this->Alertsettings);

        $this->set(compact('alertsettings'));
    }

    /**
     * View method
     *
     * @param string|null $id Alertsetting id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $alertsetting = $this->Alertsettings->get($id, [
            'contain' => []
        ]);

        $this->set('alertsetting', $alertsetting);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $alertsetting = $this->Alertsettings->newEntity();
        if ($this->request->is('post')) {
            $alertsetting = $this->Alertsettings->patchEntity($alertsetting, $this->request->getData());
            if ($this->Alertsettings->save($alertsetting)) {
                $this->Flash->success(__('Uusi hälytysryhmä tallennettu'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Ryhmää ei voitu tallentaa, yritä uudelleen.'));
        }
        $this->set(compact('alertsetting'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Alertsetting id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $alertsetting = $this->Alertsettings->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $alertsetting = $this->Alertsettings->patchEntity($alertsetting, $this->request->getData());
            if ($this->Alertsettings->save($alertsetting)) {
                $this->Flash->success(__('The alertsetting has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The alertsetting could not be saved. Please, try again.'));
        }
        $this->set(compact('alertsetting'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Alertsetting id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $alertsetting = $this->Alertsettings->get($id);
        if ($this->Alertsettings->delete($alertsetting)) {
            $this->Flash->success(__('The alertsetting has been deleted.'));
        } else {
            $this->Flash->error(__('The alertsetting could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
