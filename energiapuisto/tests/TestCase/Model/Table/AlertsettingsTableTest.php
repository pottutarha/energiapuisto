<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AlertsettingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AlertsettingsTable Test Case
 */
class AlertsettingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AlertsettingsTable
     */
    public $Alertsettings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Alertsettings'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Alertsettings') ? [] : ['className' => AlertsettingsTable::class];
        $this->Alertsettings = TableRegistry::getTableLocator()->get('Alertsettings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Alertsettings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
